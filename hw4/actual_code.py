#this code works but only for this binary! please don't use it on anything else
from scapy.all import *
from scapy.layers.http import *

capture_filename = "firmware.pcap"
filter_str = "tcp && src 192.168.86.228"
mypackets = sniff(offline = capture_filename, filter = filter_str, session=TCPSession)

output_filename = "download.bin"
import base64
f = open(output_filename, "w+b") 
i =0
packetByte = b''
for packet in mypackets:
    if packet.haslayer('Raw'):
        if("HTTP" in packet[Raw].load.decode('utf-8')[0:4]):
            
            i += 1
            #write the chunk and reset
            print(f"Writing chunk {i} length: {len(base64.b64decode(packetByte))}")
            
            f.write(base64.b64decode(packetByte))
            packetByte = b''
            
        else: 
            #join packets so we dont have padding issues
            packetByte += packet[Raw].load.split(b'\r\n')[-1] 

print(f"Finished writing! Filename: {output_filename}")
f.close()


