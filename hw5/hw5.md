# Homework 5

## Live 555 fuzzing
For this case I followed the live555 tutorial provided at https://dkmcgrath.github.io/courses/netsec/hw5.html#what-to-turn-in 

most things went smoothly, the only tricky part was getting the patch applied, 
first I had to revert the old patch ``` patch -r -p1 < $AFLNET/tutorials/live555/ceeb4f4.patch```
and then apply the patch provided. 

I also used scapy instead of wireshark to generate the .raw file to do this I used 
```
from scapy.all import *

mypackets = sniff(offline = "rtsp.pcap")
with open('output.wav' , 'wb') as f:
    for rawPacket in mypackets[Raw]:
        f.write(rawPacket.load)


```

## DNSmasq fuzzing.  
 For this I followed the tutorial on https://github.com/aflnet/aflnet/tree/master/tutorials/dnsmasq


Installing the dns server was very simple, but on the kali workstation my afl ran incredibly slow and never ended up finding a crash. 
![ScreenCap](Screenshot_100141.png)

Note the exec speed, which afl rightfully categorizes as (zzzz...)

