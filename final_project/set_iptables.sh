log_interface() {
    local interface=$1
    
    iptables -A FORWARD -m physdev --physdev-in "$interface" -j NFQUEUE --queue-num 1
    iptables -A INPUT -m physdev --physdev-in "$interface" -j NFQUEUE --queue-num 1
    iptables -A OUTPUT -m physdev --physdev-in "$interface" -j NFQUEUE --queue-num 1

}

log_interface "eth0"
log_interface "eth1"
log_interface "br0"

echo "Logging rules have been added for interfaces eth0, eth1, and br1."
