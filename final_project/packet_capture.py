import argparse
from scapy.all import sniff, wrpcap

def sniff_brige(file_name="output.pcap", max_count=None):
    if max_count is None:
        print("Capturing Packets (CTRL-C to stop)")
        packets = sniff(iface="br0")
    else:
        packets = sniff(iface="br0", count=max_count)

    wrpcap(file_name, packets)
    print(f"Packets captured and saved to {file_name}")

def main():
    parser = argparse.ArgumentParser(description="Packet Sniffer CLI using Scapy")

    parser.add_argument(
        "-f", "--file",
        type=str,
        default="output.pcap",
        help="Output file name (default: output.pcap)"
    )

    parser.add_argument(
        "-c", "--count",
        type=int,
        default=None,
        help="Number of packets to capture (default: None, capture until interrupted)"
    )

    args = parser.parse_args()

    sniff_brige(file_name=args.file, max_count=args.count)

if __name__ == "__main__":
    main()





