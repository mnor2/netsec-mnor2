from netfilterqueue import NetfilterQueue
import os
from scapy.all import *
import json
def print_and_accept(pkt):
    #print(pkt)
    mk_pkt = IP(pkt.get_payload())
    if(mk_pkt.haslayer('Raw')):
        mk_pkt[Raw].load = b''
    payload = bytes(mk_pkt)
    pkt.set_payload(payload)
    pkt.accept()
nfqueue = NetfilterQueue()
nfqueue.bind(1, print_and_accept)
try:
    nfqueue.run()
except:
    print('')

#clear_IPTABLES()
nfqueue.unbind()
